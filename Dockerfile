FROM centos:centos6
MAINTAINER <ak.hisashi@gmail.com>

ARG RUBY_VERSION="2.3.4"
ARG BITBUCKET_TOKEN=""

# タイムゾーンの変更
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
COPY docker_files/clock.txt /etc/sysconfig/clock
RUN chmod 0644 /etc/sysconfig/clock

# Perform updates
RUN yum -y update; yum clean all
RUN yum -y install \
openssl-devel \
readline-devel \
gcc \
gcc-c++ \
kernel-devel \
make \
wget \
ping \
sudo \
git \
tar \
ImageMagick-devel \
libxml2 \
libxslt \
libxml2-devel \
libxslt-devel

# yarn
RUN curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
RUN wget https://dl.yarnpkg.com/rpm/yarn.repo -O /etc/yum.repos.d/yarn.repo
RUN yum -y install yarn

# PostgreSQL9.5のインストール
RUN rpm -ivh https://download.postgresql.org/pub/repos/yum/9.5/redhat/rhel-6-x86_64/pgdg-centos95-9.5-3.noarch.rpm
RUN yum -y install \
postgresql95 \
postgresql95-contrib \
postgresql95-devel

# Rubyのインストール
RUN wget https://cache.ruby-lang.org/pub/ruby/ruby-$RUBY_VERSION.tar.gz && \
tar xvf ruby-$RUBY_VERSION.tar.gz && \
cd ruby-$RUBY_VERSION && \
./configure && \
make && \
make install && \
gem install bundler

# workman
RUN git clone https://x-token-auth:$BITBUCKET_TOKEN@bitbucket.org/kazuhisa/workman.git
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle config build.pg --with-pg-config=/usr/pgsql-9.5/bin/pg_config
RUN cd workman && bundle install --deployment --without development test && RAILS_ENV=production bundle exec rake assets:precompile

